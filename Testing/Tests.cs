﻿using NUnit.Framework;
using ProvaUf2M05egeaPol;

namespace Testing
{
    /// <summary>
    /// Classe que engloba tots els tests creats.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        /// <summary>
        /// Test que comprova si NeedFunctionName retorna bé el vector.
        /// </summary>
        [Test]
        public void NeedFunctionNameOrdenat()
        {
            int[] vector = new int[] {5, 6, 7, 8, 9, 10};
            var programa = new Program();
            int[] vector2 = programa.NeedFunctionName(5, 10);
            Assert.AreEqual(vector,vector2);
        }
        /// <summary>
        /// Test que comprova si NeedFunctionName retorna bé el vector amb n i m invertits.
        /// </summary>
        [Test]
        public void NeedFunctionNameDesordenat()
        {
            int[] vector = new int[] {9, 10, 11};
            var programa = new Program();
            int[] vector2 = programa.NeedFunctionName(11, 9);
            Assert.AreEqual(vector,vector2);
        }
        /// <summary>
        /// Test que comprova si no es posa un int vàlid al mètode.
        /// </summary>
        [Test]
        public void SelectValorNoVàlid()
        {
            var programa = new Program();
            Assert.AreEqual("No valid",programa.Select(11));
        }
        /// <summary>
        /// Test que comprova si quan es posa un nùmero insuficient al mètode.
        /// </summary>
        [Test]
        public void SelectValorInsu()
        {
            var programa = new Program();
            Assert.AreEqual("Insuficient",programa.Select(3));
        }
        /// <summary>
        /// Test que comprova si PositionsIntoString funciona quan detecta 2 cops el caràcter buscat.
        /// </summary>
        [Test]
        public void PositionsIntoString2Cops()
        {
            int[] vector = new int[] {1, 2};
            var programa = new Program();
            int[] vector2 = programa.PositionsIntoString("laal", 'a');
            Assert.AreEqual(vector,vector2);
        }
        /// <summary>
        /// Test que comprova si PositionsIntoString funciona quan no detecta el caràcter buscat.
        /// </summary>
        [Test]
        public void PositionsIntoStringCapCop()
        {
            int[] vector = new int[] {};
            var programa = new Program();
            int[] vector2 = programa.PositionsIntoString("laal", 'b');
            Assert.AreEqual(vector,vector2);
        }
        /// <summary>
        /// Test que comprova si PositionsIntoString funciona quan detecta 1 cop el caràcter buscat.
        /// </summary>
        [Test]
        public void PositionsIntoString1Cop()
        {
            int[] vector = new int[] {3};
            var programa = new Program();
            int[] vector2 = programa.PositionsIntoString("hola", 'a');
            Assert.AreEqual(vector,vector2);
        }
        /// <summary>
        /// Test que comprova si FirstOcurrencePosition retorna -1 perquè no troba el caràcter buscat.
        /// </summary>
        [Test]
        public void FirstOcurrencePositionSense()
        {
            var programa = new Program();
            Assert.AreEqual(-1,programa.FirstOccurrencePosition("hola", 'b'));
        }
        /// <summary>
        /// Test que comprova si FirstOcurrencePosition retorna la primera posició del caràcter buscat.
        /// </summary>
        [Test]
        public void FirstOcurrencePositionLletraRepetida()
        {
            var programa = new Program();
            Assert.AreEqual(3,programa.FirstOccurrencePosition("Alhambra", 'a'));
        }
    }
}