﻿using System;

namespace ProvaUf2M05egeaPol
{
    /// <summary>
    /// Classe que engloba tots els mètodes del programa.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Mètode d'inicialització del programa.
        /// </summary>
        static void Main()
        {
            
        }
            /// <summary>
            /// Aquest mètode retorna un vector de números ordenats de manera creixent a partir de m i n
            /// </summary>
            /// <param name="m">El paràmetre m és el valor inicial, en cas de que sigui més gran que n s'intercambiarà.</param>
            /// <param name="n">El paràmetre n és el valor final, en cas de que sigui més petit que m s'intercambiarà.</param>
            /// <returns>El mètode retorna el vector amb els números ordenats.</returns>
            public int[] NeedFunctionName(int m, int n)
            {
                if (m >= n) (m,n)=(n,m);
                var numero = new int[n-m+1];
                for (int i=m; i<=n;i++)
                {
                    numero[i-m]=i;
                }

                return numero;
            }
            /// <summary>
            /// El mètode retorna una qualificació en funció d'una nota obtinguda, a partir de l'enter i.
            /// </summary>
            /// <param name="i">Aquest paràmetre és l'enter que definirà la nota.</param>
            /// <returns>El mètode retorna un string amb la qualificació obtinguda</returns>
            public string Select(int i)
            {
                switch (i) 
                {
                    case 0 :
                        return "No presentat";
                    case 1 :
                    case 2 :
                    case 3 :
                    case 4 : 
                        return "Insuficient";
                    case 5 :
                    case 6 : 
                        return "Suficient";
                    case 7 :
                    case 8 :
                        return "Notable";
                    case 9 :
                        return "Excelent";
                    case 10:
                        return "Excelent + MH";
                    default:
                        return "No valid";
                }
            }
            /// <summary>
            /// Aquest mètode retorna les posicions on apareix b en el vector abc
            /// </summary>
            /// <param name="abc">Aquest paràmetre és la llista que conté els enters</param>
            /// <param name="b">Aquest paràmetre és el número a buscar</param>
            /// <returns>Retorna el vector s amb les posicions.</returns>
            public int[] PositionsIntoString(string abc, char b)
            {
                int[] s = new int[]{};
                int coincidencies = 0;
                for (int i=0; i<= abc.LastIndexOf(b);i++){
                    if (abc[i] == b)
                    {
                        if (coincidencies==0)
                        {
                            s = new int[1];
                            s[coincidencies] = i;
                            coincidencies++;
                        }
                        else
                        {
                            Array.Resize(ref s,s.Length+1);
                            s[coincidencies] = i;
                            coincidencies++;
                        }
                       
                    }
                }

                return s;
            }
            
            /// <summary>
            /// Aquest mètode serveix per a trobar la primera posició on apareix la lletra b.
            /// </summary>
            /// <param name="abc">El paràmetre abc és el string on es buscarà la lletra.</param>
            /// <param name="b">El paràmetre b és la lletra a buscar al string.</param>
            /// <returns>retorna la primera posició de la lletra o -1 en cas de no trobarla.</returns>
            public int  FirstOccurrencePosition(string abc, char b)
            {
                for (var i=0; i<abc.Length; i++) {
                    if (abc[i] == b)
                    {
                        return i;
                    }
                }

                return -1;
            }
        }
    
}